# Deploy typical web server and related software

## Requirements:

* Linux Debian 8.x
* nginx ^1.11.*
* PHP ^5.6.*
* PostgreSQL ^9.5
* Redis ^3.2.*
* ElasticSearch ^2.3.*

## Resources:

* 3 dedicated servers in common data center
* one of servers has low RAM and processor resource
* one of servers has high-performance processor, limited disk space and average value of RAM
* one of servers has average processor, high RAM and large disk space

## Goal:

* Present architecture as diagram
* Deploy NLB as one point access
* File/media storage
* Databases storage (with cache and search machine)
* Web server for high-load API
* All servers have to be bonded by ssh tunnels 
* Write examples of cli instruction to deploy software and architecture (use `apt` to install only)